#!/env/python
# coding: utf-8

from flask import Flask, render_template, request, jsonify, redirect, url_for

import numpy as np
from PIL import Image
import base64
import re
import io
import random

import os
from os.path import join
from subprocess import call

app = Flask(__name__)

# @app.route('/<x>/<y>')
# def index(x, y):
#     return render_template('index.html', x = 0, y = 0)


def compose_first_line(x):
    dout = 'out'
    din = join('static', 'init')
    file_name = '0000_%04d.png'%(x-1)    
    
    tmp_dir = 'tmp%s'%random.random()

    init_orig = join('static', 'images', 'init.png')
    crop_out = join(tmp_dir, 'crop.png')
    init_crop = join(tmp_dir, 'init_crop.png')

    composite_out = join(din, '0000_%04d.png'%x)
    
    os.mkdir(tmp_dir)
    call(['convert', '-crop', '200x1200+1000+0', join(dout, file_name), crop_out])
    call(['convert', '-crop', '1000x1200+200+0', init_orig, init_crop])

    call(['convert', crop_out, init_crop, '+append', composite_out])


@app.route('/')
def rien():
    return 'hello gs'

@app.route('/<coord>')
def index(coord):
    coords = coord.split('_')
    xx = int(coords[0])
    yy = int(coords[1])
    # if (xx > 0):
    #     # create init
    #     if (yy == 0):
    #         compose_first_line(xx)
            
    return render_template('index.html', x = coords[0], y = coords[1])


@app.route('/hook', methods=['POST'])
def get_image():
    image_b64 = request.values['imageBase64']
    coord_x = int(request.values['coord_x'])
    coord_y = int(request.values['coord_y'])
    
    image_data = base64.b64decode(re.sub('^data:image/.+;base64,', '', image_b64))
    print(image_b64)
    file_name = 'out/%04d_%04d.png'%(coord_y, coord_x)
    image_PIL = Image.open(io.BytesIO(image_data))
    image_PIL.save(file_name, 'PNG')
    image_np = np.array(image_PIL)
    print('Image received: {}'.format(image_np.shape))
    print(int(request.values['coord_x']))
    return ''

if __name__ == '__main__':
    app.run(debug=True)
    
